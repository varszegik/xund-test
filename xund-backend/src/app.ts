import * as AWS from 'aws-sdk';
import { json } from 'body-parser';
import express from 'express';
import pino from 'express-pino-logger';
import requestIp from 'request-ip';
import { v4 } from 'uuid';
import { config } from './config';
import { imagesRepoSQLFactory } from './domain/image/image.repo';
import { imageGetUploadUrlEndpointFactory } from './endpoint/image/image-get-upload-url.endpoint';
import { listImagesEndpointFactory } from './endpoint/image/list-images.endpoint';
import { uploadImageEndpointFactory } from './endpoint/image/upload-image.endpoint';
import { testEndpointFactory } from './endpoint/status';
import { registerEndpoints } from './framework/endpoint/endpoint-register-express';
import { createRunQuery } from './framework/sql/create-run-query';
import { poolFactory } from './framework/sql/pool.factory';
import { runInTransactionFactory } from './framework/sql/run-in-transaction';
import { s3ServiceS3Factory } from './framework/storage/storage-service-s3';
import { sqlTransactionServiceFactory } from './framework/transaction/sql-transaction.service';
import { imageGetUploadUrlUseCaseFactory } from './usecase/images/image-get-upload-url.use-case';
import { listImagesUseCaseFactory } from './usecase/images/list-images.use-case';
import { uploadImageUseCaseFactory } from './usecase/images/upload-image.use-case';

const app = express();
app.use(pino());
app.use(requestIp.mw());
app.use(json());

const s3 = new AWS.S3({
  accessKeyId: config.AWSAccessKeyId,
  secretAccessKey: config.AWSSecretKey,
  region: 'eu-central-1',
  s3ForcePathStyle: true,
  signatureVersion: 'v4',
  sslEnabled: false,
});

const pool = poolFactory();
const transactionService = sqlTransactionServiceFactory({ pool });
const runQuery = createRunQuery(transactionService);

const storageService = s3ServiceS3Factory({ s3, urlExpirationSeconds: 86000 });
storageService.createPublicBucketIfNotExists(config.s3BucketName);

const transacting = runInTransactionFactory({
  transactionService,
});

const imagesRepo = imagesRepoSQLFactory(runQuery);

const listImagesUseCase = transacting(
  listImagesUseCaseFactory({
    imagesRepo,
  }),
);
const imageGetUploadUrlUseCase = imageGetUploadUrlUseCaseFactory({ storageService, generateId: v4 });
const uploadImageUseCase = transacting(uploadImageUseCaseFactory({ imagesRepo }));

const testEndpoint = testEndpointFactory();

const listImagesEndPoint = listImagesEndpointFactory({ listImagesUseCase });
const imageGetUploadUrlEndpoint = imageGetUploadUrlEndpointFactory({ imageGetUploadUrlUseCase });
const uploadImageEndpoint = uploadImageEndpointFactory({ uploadImageUseCase });

registerEndpoints(app, [testEndpoint, listImagesEndPoint, imageGetUploadUrlEndpoint, uploadImageEndpoint]);

app.listen(process.env.PORT ?? 3001);
