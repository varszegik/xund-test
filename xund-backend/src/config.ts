import convict from 'convict';

const configObject = convict({
  databaseUrl: {
    doc: 'The URL of the PostgreSQL database',
    format: String,
    default: 'http://user:password@localhost:5432/db',
    env: 'DATABASE_URL',
  },
  databaseUseSSL: {
    doc: 'Use SSL when connecting to database',
    format: Boolean,
    default: false,
    env: 'DATABASE_USE_SSL',
  },
  s3BucketName: {
    doc: 'The name of the s3 bucket',
    format: String,
    default: 'xund',
    env: 'S3_BUCKET_NAME',
  },
  AWSAccessKeyId: {
    doc: 'AWS Access key ID',
    format: String,
    default: 'invalid-change',
    env: 'AWS_ACCESS_KEY_ID',
  },
  AWSSecretKey: {
    doc: 'AWS Secret Key',
    format: String,
    default: 'invalid-change',
    env: 'AWS_SECRET_KEY',
  },
});

configObject.validate({ allowed: 'warn' });

export const config = configObject.getProperties();
export type Config = typeof config;
