import { PoolClient } from 'pg';
import { Transaction } from './transaction';

export interface TransactionService {
  begin(): Promise<Transaction>;
  poolClient: PoolClient;
}
