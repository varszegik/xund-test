import { Handler } from 'express';
import { Endpoint } from './endpoint';
import { expressRequestContextFactory } from './request/request-express';

export const endpointToExpressRoute =
  <
    TReqBody extends Record<string, unknown> = Record<string, unknown>,
    TResBody extends Record<string, unknown> = Record<string, unknown>,
    TReqQuery extends Record<string, string> = Record<string, string>,
    TReqParams extends Record<string, string> = Record<string, string>,
  >(
    endpoint: Endpoint<TReqBody, TResBody, TReqQuery, TReqParams>,
  ): Handler =>
  async (req, res) => {
    const endpointContext = expressRequestContextFactory<TReqBody, TReqQuery, TReqParams>(req);
    const { body, status } = await endpoint.handler(endpointContext);

    return res.status(status).send(body);
  };
