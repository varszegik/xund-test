import { Express } from 'express';
import { Endpoint } from './endpoint';
import { endpointToExpressRoute } from './endpoint-to-express-route';

export const registerEndpoints = (server: Express, endpoints: Endpoint<any, any, any, any>[]) => {
  endpoints.forEach((endpoint) => {
    server[endpoint.method](endpoint.route, endpointToExpressRoute(endpoint));
  });
};
