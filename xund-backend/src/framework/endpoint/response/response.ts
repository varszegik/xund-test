export type Response<
  TBody extends Record<string, unknown> = Record<string, unknown>
> = {
  status: number;
  body: TBody;
  headers?: { [key: string]: string };
};
