import { EndpointMethods } from "./endpoint-methods";
import { RequestContext } from "./request/request";
import { Response } from "./response/response";

export interface Endpoint<
  TReqBody extends Record<string, unknown> = Record<string, unknown>,
  TResBody extends Record<string, unknown> = Record<string, unknown>,
  TQuery extends Record<string, string> = Record<string, string>,
  TParams extends Record<string, string> = Record<string, string>
> {
  method: EndpointMethods;
  route: string;
  handler: (
    request: RequestContext<TReqBody, TQuery, TParams>
  ) => Promise<Response<TResBody>>;
}
