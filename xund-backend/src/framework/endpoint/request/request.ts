export type RequestContext<TBody, TQuery, TParams> = {
  method: string;
  path: string;
  body: TBody;
  query: TQuery;
  parameters: TParams;
  headers: { [key: string]: string | string[] };
  clientIp: string;
};
