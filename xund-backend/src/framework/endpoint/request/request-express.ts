import { Request } from 'express';
import { RequestContext } from './request';

export const expressRequestContextFactory = <
  TBody extends Record<string, unknown>,
  TQuery extends Record<string, string> = Record<string, string>,
  TParams extends Record<string, string> = Record<string, string>,
>(
  request: Request,
): RequestContext<TBody, TQuery, TParams> =>
  ({
    query: request.query ?? {},
    parameters: request.params ?? {},
    headers: request.headers ?? {},
    body: request.body ?? { help: 'asd' },
    clientIp: request.clientIp,
  } as RequestContext<TBody, TQuery, TParams>);
