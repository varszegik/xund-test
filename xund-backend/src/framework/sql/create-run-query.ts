import { QueryResult } from 'pg';
import { TransactionService } from '../transaction/transaction.service';

type RunQueryInput = { text: string; values: unknown[] };
export type RunQuery = (input: RunQueryInput) => Promise<QueryResult>;

export function createRunQuery(sqlTransactionService: TransactionService): RunQuery {
  return ({ text, values }) => {
    return sqlTransactionService.poolClient.query(text, values);
  };
}
