import { runInContext } from '../cls-hooked/run-in-context';
import { TransactionService } from '../transaction/transaction.service';

export const runInTransactionFactory =
  ({ transactionService }: { transactionService: TransactionService }) =>
  <Input, Output>(fn: (input: Input) => Promise<Output>) =>
  (input: Input): Promise<Output> =>
    runInContext(async () => {
      const transaction = await transactionService.begin();
      try {
        const result = await fn(input);
        await transaction.commit();
        return result;
      } catch (error) {
        await transaction.rollback();
        throw error;
      }
    });
