import { Endpoint } from '../../framework/endpoint/endpoint';
import { EndpointMethods } from '../../framework/endpoint/endpoint-methods';
import {
  ImageGetUploadUrlUseCase,
  ImageGetUploadUrlUseCaseInput,
  ImageGetUploadUrlUseCaseOutput,
} from '../../usecase/images/image-get-upload-url.use-case';

type ImageGetUploadUrlRequestBody = {};
type ImageGetUploadUrlResponseBody = ImageGetUploadUrlUseCaseOutput;
type ImageGetUploadUrlQueryParams = ImageGetUploadUrlUseCaseInput;
type ImageGetUploadUrlRequestParams = {};

export type ImageGetUploadUrlEndpoint = Endpoint<
  ImageGetUploadUrlRequestBody,
  ImageGetUploadUrlResponseBody,
  ImageGetUploadUrlQueryParams,
  ImageGetUploadUrlRequestParams
>;
/**
 * @param  imageGetUploadUrlUseCase dependency injected use case, which handles creating a presigned url for uploading images
 * factory for creating an endpoint GET /images-url which returns a presigned URL, where an image can be uploaded to, with a PUT request
 */
export const imageGetUploadUrlEndpointFactory = ({
  imageGetUploadUrlUseCase,
}: {
  imageGetUploadUrlUseCase: ImageGetUploadUrlUseCase;
}): ImageGetUploadUrlEndpoint => ({
  method: EndpointMethods.GET,
  route: '/images-url',
  handler: async (input) => {
    const useCaseResult = await imageGetUploadUrlUseCase(input.query);
    return {
      body: useCaseResult,
      status: 200,
    };
  },
});
