import { Endpoint } from '../../framework/endpoint/endpoint';
import { EndpointMethods } from '../../framework/endpoint/endpoint-methods';
import { UploadImageUseCase } from '../../usecase/images/upload-image.use-case';

type UploadImageRequestBody = {
  description: string;
  filename: string;
  size: string;
  source: string;
  dimensions: string;
};
type UploadImageResponseBody = {};
type UploadImageQueryParams = {};
type UploadImageRequestParams = {};

export type UploadImageEndpoint = Endpoint<
  UploadImageRequestBody,
  UploadImageResponseBody,
  UploadImageQueryParams,
  UploadImageRequestParams
>;
/**
 * @param  uploadImageUseCase dependency injected use case, which handles saving image details
 * factory for creating an endpoint POST /images which handles the logic behind storing the images
 */
export const uploadImageEndpointFactory = ({
  uploadImageUseCase,
}: {
  uploadImageUseCase: UploadImageUseCase;
}): UploadImageEndpoint => ({
  method: EndpointMethods.POST,
  route: '/images',
  handler: async (input) => {
    const useCaseResult = await uploadImageUseCase({ image: { ...input.body, ip: input.clientIp } });
    return {
      body: useCaseResult,
      status: 200,
    };
  },
});
