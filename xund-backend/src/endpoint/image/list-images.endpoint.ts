import { Endpoint } from '../../framework/endpoint/endpoint';
import { EndpointMethods } from '../../framework/endpoint/endpoint-methods';
import { ListImagesUseCase, ListImagesUseCaseOutput } from '../../usecase/images/list-images.use-case';

type GetImagesRequestBody = {};
type GetImagesResponseBody = ListImagesUseCaseOutput;
type GetImagesQueryParams = {};
type GetImagesRequestParams = {};

export type GetImagesEndpoint = Endpoint<
  GetImagesRequestBody,
  GetImagesResponseBody,
  GetImagesQueryParams,
  GetImagesRequestParams
>;
/**
 * @param  listImagesUseCase dependency injected use case, which handles listing images business logic
 * factory for creating an endpoint GET /images which returns a list of uploaded images
 */
export const listImagesEndpointFactory = ({
  listImagesUseCase,
}: {
  listImagesUseCase: ListImagesUseCase;
}): GetImagesEndpoint => ({
  method: EndpointMethods.GET,
  route: '/images',
  handler: async (input) => {
    const useCaseResult = await listImagesUseCase(input);
    return {
      body: useCaseResult,
      status: 200,
    };
  },
});
