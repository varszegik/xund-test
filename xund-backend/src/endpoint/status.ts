import { Endpoint } from '../framework/endpoint/endpoint';
import { EndpointMethods } from '../framework/endpoint/endpoint-methods';

export type TestEndpointResponseBody = {
  status: 'ok';
  timestamp: number;
  output: Promise<Record<string, unknown>>;
};
export type GetTestEndpoint = Endpoint<
  Record<string, unknown>,
  Record<string, unknown>,
  Record<string, string>,
  Record<string, string>
>;
/**
 * Factory for creating an endpoint for GET /test
 * this endpoint is mainly user for testing if the API works
 */
export const testEndpointFactory = (): GetTestEndpoint => ({
  method: EndpointMethods.GET,
  route: '/test',
  handler: async () => {
    return {
      body: { output: { output: 'test' }, status: 'ok', timestamp: Date.now() },
      status: 200,
    };
  },
});
