import { StorageService } from '../../framework/storage/storage-service';
import { AsyncUseCase } from '../use-case';

export type ImageGetUploadUrlUseCaseInput = {
  fileType: string;
};

export type ImageGetUploadUrlUseCaseOutput = {
  url: string;
};

export type ImageGetUploadUrlUseCase = AsyncUseCase<ImageGetUploadUrlUseCaseInput, ImageGetUploadUrlUseCaseOutput>;
/**
 * @param  storageService dependency injected service for handling image storage bucket creation and URL signing
 * @param  generateId dependency injected function that generates a unique identifier
 * @returns ImageGetUploadUrlUseCase use case, which handles creating a presigned url for uploading images
 * factory that creates a Use Case that returns a presigned url for uploading images
 */
export const imageGetUploadUrlUseCaseFactory =
  ({
    storageService,
    generateId,
  }: {
    storageService: StorageService;
    generateId: () => string;
  }): ImageGetUploadUrlUseCase =>
  async ({ fileType }) => {
    const url = await storageService.getSignedURLForUpload('xund', generateId(), fileType);
    return { url };
  };
