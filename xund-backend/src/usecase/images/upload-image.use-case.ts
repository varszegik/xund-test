import { v4 } from 'uuid';
import { Image } from '../../domain/image/image';
import { ImagesRepo } from '../../domain/image/image.repo';
import { AsyncUseCase } from '../use-case';

export type UploadImageUseCaseInput = {
  image: Omit<Image, 'id' | 'uploadedAt'>;
};

export type UploadImageUseCaseOutput = {
  image: Image;
};

export type UploadImageUseCase = AsyncUseCase<UploadImageUseCaseInput, UploadImageUseCaseOutput>;
/**
 * @param  imagesRepo dependency injected data access layer for images
 * Returns a use case that saves an image through the injected data access layer
 */
export const uploadImageUseCaseFactory =
  ({ imagesRepo }: { imagesRepo: ImagesRepo }): UploadImageUseCase =>
  async ({ image }) => {
    const imageFromDb = await imagesRepo.addImage({ ...image, id: v4() });
    return {
      image: imageFromDb,
    };
  };
