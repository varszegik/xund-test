import { Image } from '../../domain/image/image';
import { imagesRepoMockFactory } from '../../domain/image/image-repo.mock';
import { listImagesUseCaseFactory } from './list-images.use-case';

describe('list-images', () => {
  it('should return a list of images', async () => {
    const imagesRepo = imagesRepoMockFactory();
    const images: Image[] = [];
    imagesRepo.getImages = async () => images;
    const listImagesUseCase = listImagesUseCaseFactory({ imagesRepo });
    const res = await listImagesUseCase({});
    expect(res).toEqual({ images });
  });
});
