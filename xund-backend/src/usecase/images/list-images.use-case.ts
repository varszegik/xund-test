import { Image } from '../../domain/image/image';
import { ImagesRepo } from '../../domain/image/image.repo';
import { AsyncUseCase } from '../use-case';

export type ListImagesUseCaseInput = {};

export type ListImagesUseCaseOutput = {
  images: Image[];
};

export type ListImagesUseCase = AsyncUseCase<ListImagesUseCaseInput, ListImagesUseCaseOutput>;
/**
 * @param  imagesRepo dependency injected data access layer for images
 * Returns a use case that returns a list of images
 */
export const listImagesUseCaseFactory =
  ({ imagesRepo }: { imagesRepo: ImagesRepo }): ListImagesUseCase =>
  async () => {
    const res = await imagesRepo.getImages();

    return {
      images: res,
    };
  };
