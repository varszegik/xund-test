import { ImagesRepo } from './image.repo';
/**
 * Throws Not implemented exception
 */
const throwNotImplemented = () => {
  throw new Error('Not implemented');
};
/**
 * @returns ImagesRepo mocked image data access layer
 * Creates an ImagesRepo with mocked functions
 */
export const imagesRepoMockFactory = (): ImagesRepo => {
  return { addImage: throwNotImplemented, getImages: throwNotImplemented };
};
