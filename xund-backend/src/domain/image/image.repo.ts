import { RunQuery } from '../../framework/sql/create-run-query';
import { Image } from './image';

export interface ImagesRepo {
  getImages(): Promise<Image[]>;
  addImage(image: Omit<Image, 'uploadedAt'>): Promise<Image>;
}

export const imagesRepoSQLFactory = (runQuery: RunQuery): ImagesRepo => {
  /**
   * Returns a list of images ordered by upload date, descending
   */
  const getImages = async () => {
    const res = await runQuery({ text: 'SELECT * FROM images ORDER BY "uploadedAt" DESC', values: [] });
    return res.rows;
  };
  /**
   * @param  image image to be added to the images table
   * Stores the image from argument in the images table, and returns it.
   */
  const addImage = async (image: Image) => {
    const res = await runQuery({
      text: `
              INSERT INTO images("id", "filename", "size", "ip", "dimensions", "source", "description") 
              VALUES ($1, $2, $3, $4, $5, $6, $7)
              RETURNING *
            `,
      values: [image.id, image.filename, image.size, image.ip, image.dimensions, image.source, image.description],
    });
    return res.rows[0];
  };
  return {
    getImages,
    addImage,
  };
};
