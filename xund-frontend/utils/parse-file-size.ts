export const parseFileSize = (fileSize: string) => {
  return `${(parseFloat(fileSize) / 1024 / 1024).toFixed(2)} MB`;
};
