export const getImageDimensions = async (file: File): Promise<{ width: number; height: number }> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      const image = new Image();
      image.src = event.target?.result as string;
      image.onload = () => {
        const width = image.naturalWidth;
        const height = image.naturalHeight;
        resolve({ width, height });
      };
      image.onerror = reject;
    };
    reader.onerror = reject;
  });
};
