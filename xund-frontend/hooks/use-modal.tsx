import { createContext, FC, useCallback, useContext, useState } from 'react';
import Modal from '../components/component-library/modal/modal';

type OpenModalOptions = {
  content?: JSX.Element | string;
};

const ModalContext = createContext<{ openModal: (options: OpenModalOptions) => void; closeModal: () => void }>({
  openModal: () => {
    return;
  },
  closeModal: () => {},
});

export const useModal = (): { openModal: (options: OpenModalOptions) => void; closeModal: () => void } =>
  useContext(ModalContext);

export const ModalProvider: FC = ({ children }) => {
  const [modalState, setModalState] = useState<OpenModalOptions | null>(null);

  const openModal = useCallback(
    (options: OpenModalOptions): void => {
      setModalState(options);
    },
    [setModalState],
  );

  const handleClose = useCallback((): void => {
    setModalState(null);
  }, []);

  return (
    <>
      <ModalContext.Provider value={{ openModal, closeModal: handleClose }}>
        {children}
        <Modal open={!!modalState} onClose={handleClose}>
          {modalState?.content}
        </Modal>
      </ModalContext.Provider>
    </>
  );
};
