/** @type {import('next').NextConfig} */

const backendURL = process.env.NEXT_PUBLIC_BACKEND_URL ?? '';

module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['s3.eu-central-1.amazonaws.com'],
  },
  rewrites: async () => {
    return [
      {
        source: '/api/:path*',
        destination: `${backendURL}/:path*`,
      },
    ];
  },
};
