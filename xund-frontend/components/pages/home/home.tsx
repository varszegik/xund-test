import { NextPage } from 'next';
import React, { useCallback } from 'react';
import { useModal } from '../../../hooks/use-modal';
import PageFrame from '../../component-library/page-frame/page-frame';
import Picture from '../../component-library/picture/picture';
import UploadImageModal from '../../component-library/upload-image-modal/upload-image-modal';
import UploadImage from '../../component-library/upload-image/upload-image';
import classes from './home.module.scss';
import { useListImages } from './hooks/use-list-images';
import { Image } from './types/image';

export const Home: NextPage<{ images: Image[] }> = ({ images: ssrImages }) => {
  const { openModal, closeModal } = useModal();

  const openUploadModal = useCallback(() => {
    openModal({ content: <UploadImageModal onClose={closeModal} /> });
  }, [openModal, closeModal]);

  const { images } = useListImages(ssrImages);

  return (
    <PageFrame>
      <header className={classes.heading2}>Uploaded Images</header>
      <article className={classes.images}>
        <div className={classes.uploadImageWrapper}>
          <UploadImage onClick={openUploadModal} />
        </div>
        {images.map((image) => (
          <Picture
            key={image.id}
            description={image.description}
            dimensions={image.dimensions}
            filename={image.filename}
            size={image.size}
            source={image.source}
            ip={image.ip}
            uploadedAt={image.uploadedAt}
          />
        ))}
      </article>
    </PageFrame>
  );
};
