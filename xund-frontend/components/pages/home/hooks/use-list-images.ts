import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../store/store';
import { listImages, setImages } from '../store/actions';
import { Image } from '../types/image';

export const useListImages = (ssrImages: Image[]) => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (ssrImages) {
      dispatch(setImages({ images: ssrImages }));
    } else {
      dispatch(listImages());
    }
  }, [dispatch, ssrImages]);

  const images = useSelector((state: RootState) => state.home.images);
  return { ...images, images: images.images ?? ssrImages ?? [] };
};
