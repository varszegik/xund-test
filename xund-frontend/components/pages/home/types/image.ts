export type Image = {
  id: string;
  description: string;
  dimensions: string;
  filename: string;
  size: string;
  ip: string;
  source: string;
  uploadedAt: string;
};
