import { createReducer } from '@reduxjs/toolkit';
import { Image } from '../types/image';
import { listImages, setImages, uploadImage } from './actions';

type IHomeState = {
  images: { images: Image[] | null; loading: number; error: boolean };
};

const initialHomeState: IHomeState = {
  images: {
    images: null,
    loading: 0,
    error: false,
  },
};

export const homeReducer = createReducer(initialHomeState, (builder) => {
  builder
    .addCase(setImages, (state, action) => {
      state.images.images = action.payload.images;
    })
    .addCase(listImages.pending, (state, action) => {
      state.images.loading++;
    })
    .addCase(listImages.fulfilled, (state, action) => {
      state.images.images = action.payload?.images ? action.payload.images : [];
      state.images.loading--;
      state.images.error = false;
    })
    .addCase(listImages.rejected, (state, action) => {
      state.images.loading--;
      state.images.error = true;
      state.images.images = null;
    })
    .addCase(uploadImage.fulfilled, (state, action) => {
      if (action.payload?.image) {
        state.images.images?.unshift(action.payload?.image);
      }
    });
});
