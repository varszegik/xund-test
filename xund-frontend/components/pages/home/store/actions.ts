import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { Image } from '../types/image';

enum HomeActions {
  SET_IMAGES = 'SET_IMAGES',
  LIST_IMAGES = 'LIST_IMAGES',
  UPLOAD_IMAGE = 'UPLOAD_IMAGE',
}

export const setImages = createAction<{ images: Image[] }>(HomeActions.SET_IMAGES);

export const listImages = createAsyncThunk<{ images: Image[] } | undefined, void>(HomeActions.LIST_IMAGES, async () => {
  try {
    const res = await axios.get<{ images: Image[] }>('/api/images');
    return res.data;
  } catch (err) {
    console.error(err);
  }
});

export const uploadImage = createAsyncThunk<
  { image: Image } | undefined,
  { image: Omit<Image, 'id' | 'uploadedAt' | 'ip'> }
>(HomeActions.UPLOAD_IMAGE, async ({ image }) => {
  try {
    const res = await axios.post<{ image: Image }>('/api/images', image);
    return res.data;
  } catch (err) {
    console.error(err);
  }
});
