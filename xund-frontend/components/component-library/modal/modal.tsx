import React, { FC } from 'react';
import classes from './modal.module.scss';

type ModalProps = {
  open: boolean;
  onClose: () => void;
};

const Modal: FC<ModalProps> = ({ onClose, open, children }) => {
  if (!open) {
    return <></>;
  }
  return (
    <div
      className={classes.modalBackground}
      onClick={(event) => {
        if (event.target !== event.currentTarget) {
          return;
        }
        onClose();
      }}
    >
      <div className={classes.contentWrapper} onClick={() => {}}>
        {children}
      </div>
    </div>
  );
};

export default Modal;
