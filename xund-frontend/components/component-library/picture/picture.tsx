import React, { FC } from 'react';
import PictureDetails from '../picture-details/picture-details';
import classes from './picture.module.scss';

type PictureProps = {
  source: string;
  filename: string;
  size: string;
  ip: string;
  uploadedAt: string;
  dimensions: string;
  description: string;
};

const Picture: FC<PictureProps> = (props) => {
  return (
    <div className={classes.pictureWrapper}>
      <div className={classes.details}>
        <PictureDetails {...props} />
      </div>
      <img className={classes.picture} src={props.source} alt="Image uploaded by user" />
    </div>
  );
};

export default Picture;
