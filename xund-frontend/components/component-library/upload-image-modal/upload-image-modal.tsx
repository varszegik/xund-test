import axios from 'axios';
import { Field, FieldProps, Form, Formik, FormikProps } from 'formik';
import React, { FC, useRef } from 'react';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';
import { getImageDimensions } from '../../../utils/get-image-dimensions';
import { uploadImage } from '../../pages/home/store/actions';
import UploadImage from '../upload-image/upload-image';
import classes from './upload-image-modal.module.scss';

type UploadImageModalProps = {
  onClose: () => void;
};

const ALLOWED_FILE_TYPES = ['image/jpeg', 'image/png'];

const UploadImageModal: FC<UploadImageModalProps> = ({ onClose }) => {
  const dispatch = useDispatch();
  const fileInputRef = useRef<HTMLInputElement>(null);

  return (
    <Formik
      initialValues={{
        image: {} as FileList,
        description: '',
      }}
      validationSchema={yup.object().shape({
        image: yup
          .object({
            '0': yup.object().required('Please select an image'),
          })
          .nullable(),
        describe: yup.string(),
      })}
      onSubmit={async (values) => {
        const file = values.image[0];
        if (!ALLOWED_FILE_TYPES.includes(file.type)) {
          return onClose();
        }
        const dimensions = await getImageDimensions(file);
        const {
          data: { url },
        } = await axios.get<{ url: string }>('/api/images-url', {
          params: { fileType: file.type },
        });
        await axios.put(url.replace('http://', 'https://'), file);
        dispatch(
          uploadImage({
            image: {
              source: url.split('?')[0],
              filename: file.name,
              size: file.size.toString(),
              description: values.description,
              dimensions: `${dimensions.width} x ${dimensions.height}`,
            },
          }),
        );
        onClose();
      }}
    >
      {({ isValid, dirty }: FormikProps<{}>) => (
        <Form>
          <div className={classes.modalRoot}>
            <div className={classes.heading3}>New Image</div>
            <div className={classes.plusButtonWrapper}>
              <Field>
                {({ form: { setFieldValue, errors } }: FieldProps) => (
                  <>
                    <input
                      name="image"
                      type="file"
                      ref={fileInputRef}
                      style={{ display: 'none' }}
                      onChange={(event) => setFieldValue('image', event.target.files)}
                      max={1}
                      accept={ALLOWED_FILE_TYPES.join(', ')}
                    />
                    <p className={classes.error}>{errors.image}</p>
                  </>
                )}
              </Field>
              <div className={classes.plusButton}>
                <UploadImage onClick={() => fileInputRef?.current?.click()} />
              </div>
              <div className={classes.caption1}>only jpg or png</div>
            </div>
            <div className={classes.descriptionInputWrapper}>
              <div className={classes.body1}>Description</div>
              <Field component="textarea" name="description" rows={10} className={classes.descriptionTextArea}></Field>
            </div>
            <div className={classes.modalFooter}>
              <div className={classes.caption2}>
                By uploading an image, you accept{' '}
                <a target="_blank" rel="noreferrer" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                  our Terms
                </a>
              </div>
              <button className={classes.button} type="submit" disabled={!isValid || !dirty}>
                <div className={classes.buttonText}>Save</div>
              </button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default UploadImageModal;
