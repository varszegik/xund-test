import Image from 'next/image';
import React, { FC } from 'react';
import classes from './page-frame.module.scss';

interface PageFrameProps {}

const PageFrame: FC<PageFrameProps> = ({ children }) => {
  return (
    <>
      <header className={classes.header}>
        <Image src="/lime.svg" alt="Vercel Logo" width={30} height={30} />
        <span className={classes.headerTitle}>LimeCRM</span>
      </header>
      <main className={classes.pageBody}>{children}</main>
    </>
  );
};

export default PageFrame;
