import React, { FC } from 'react';
import classes from './upload-image.module.scss';

type UploadImageProps = {
  onClick: () => void;
};

const UploadImage: FC<UploadImageProps> = ({ onClick }) => {
  return (
    <div className={classes.uploadImageRoot} onClick={onClick}>
      <div className={classes.imageButton}>
        <div className={classes.plusSign} />
      </div>
    </div>
  );
};

export default UploadImage;
