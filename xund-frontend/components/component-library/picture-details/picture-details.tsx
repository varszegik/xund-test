import * as datefns from 'date-fns';
import Image from 'next/image';
import React, { FC } from 'react';
import { parseFileSize } from '../../../utils/parse-file-size';
import classes from './picture-details.module.scss';

type PictureDetailsProps = {
  filename: string;
  size: string;
  ip: string;
  uploadedAt: string;
  dimensions: string;
  description: string;
  source: string;
};

const InformationPiece: FC<{ label: string; value: string }> = ({ label, value }) => {
  return (
    <div className={classes.informationPiece}>
      <div className={classes.label}>{label}:</div>
      <div className={classes.value}>{value}</div>
    </div>
  );
};

const PictureDetails: FC<PictureDetailsProps> = ({
  description,
  uploadedAt,
  size,
  filename,
  dimensions,
  ip,
  source,
}) => {
  return (
    <div className={classes.details}>
      <div className={classes.information}>
        <div className={classes.texts}>
          <div className={classes.title}>{filename}</div>
          <InformationPiece label="size" value={parseFileSize(size)} />
          <InformationPiece label="source" value={ip} />
          <InformationPiece label="uploaded at" value={datefns.format(new Date(uploadedAt), 'yyyy.MM.dd. HH:mm:ss')} />
          <InformationPiece label="dimensions" value={dimensions} />
        </div>
        <div className={classes.copyLink} onClick={() => navigator.clipboard.writeText(source)}>
          <Image src="/link.svg" alt="Copy link to clipboard" width={30} height={30} />
        </div>
      </div>
      <div className={classes.description}>
        <div className={classes.descriptionTitle}>Description</div>
        <div className={classes.descriptionContent}>{description}</div>
      </div>
    </div>
  );
};

export default PictureDetails;
