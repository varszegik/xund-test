import type { AppProps } from 'next/app';
import Head from 'next/head';
import React from 'react';
import { Provider } from 'react-redux';
import { ModalProvider } from '../hooks/use-modal';
import { store } from '../store/store';
import '../styles/globals.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Lime CRM</title>
      </Head>
      <Provider store={store}>
        <ModalProvider>
          <Component {...pageProps} />
        </ModalProvider>
      </Provider>
    </>
  );
}
export default MyApp;
