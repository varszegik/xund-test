import axios from 'axios';
import { GetServerSideProps } from 'next';
import { Home } from '../components/pages/home/home';
import { Image } from '../components/pages/home/types/image';

export default Home;

export const getServerSideProps: GetServerSideProps = async () => {
  const res = await axios.get<{ images: Image[] }>(`${process.env.NEXT_PUBLIC_BACKEND_URL}/images`);
  return {
    props: {
      ...res.data,
    },
  };
};
